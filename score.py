# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''
import cocos
from cocos.layer.base_layers import Layer
from cocos.sprite import Sprite

# Classe que cuida da pontuação do jogo
class Score(Layer):
    def __init__(self, score):
        super(Score, self).__init__()
        self.label = cocos.text.Label(
            "Score: "+str(score),
            font_name='arial',
            bold=True,
            color=(200,0,70,255),
            font_size=20,
            anchor_x='left', anchor_y='center',
            dpi=70
        )
        self.label.position = 10, 30
        self.add(self.label, z=0)

    def scoreUpdate(self, score):
        self.remove(self.label)    
        self.label = cocos.text.Label(
            "Score: "+str(score),
            font_name='arial',
            bold=True,
            color=(200,0,70,255),
            font_size=20,
            anchor_x='left', anchor_y='center',
            dpi=70
        )
        self.label.position = 10, 30
        self.add(self.label, z=0)