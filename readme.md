# 3º dia Protocalouros

Módulos para criar o jogo para o 3º dia do Protocalouros

## Rodando

python game.py

## Dependências

É necessário ter a biblioteca pyglet para rodar o jogo

## Módulo Main_Handler

Encapsula as funções necessárias para as tarefas do jogo.

##### - createWindow(titulo)

Cria uma janela vazia, o nome no topo da janela é o parâmetro da função.

##### - createBackground()

Cria a camada do fundo com a imagem passada no módulo.

##### - createCharacter()

Cria a camada com o personagem.

##### - createFruits()

Cria a camada na qual as frutas caem.

##### - startCollision()

Inicia o sistema de colisão.

##### - checkCollision()

Checa se fruta colide com o personagem.

## Módulo Window

Código para criar uma janela.

## Módulo Background

Carrega a imagem de fundo e a coloca em uma camada inicial para a cena.

## Módulo Character

Carrega a imagem do personagem e coloca numa camada acima do background.

## Módulo Fruits

Carrega as imagens das frutas e faz elas caírem.