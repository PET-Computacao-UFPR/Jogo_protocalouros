# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''
from cocos.layer.base_layers import Layer

# Classe da janela, encapsula um "layer vazio" do jogo
class GameWindow(Layer):
    def __init__(self):
        super(GameWindow, self).__init__()
