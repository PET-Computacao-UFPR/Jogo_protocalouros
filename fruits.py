# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''

from cocos.layer.base_layers import Layer
from cocos.sprite import Sprite
from cocos.collision_model import AARectShape
from random import randint

# Classe de uma fruta, é passado o nome da imagem e suas dimensões
class Fruit():
    def __init__(self, sprite, width, height, quad, y):
        # aumente o numero de quadrantes para fazer as frutas caírem mais próximas umas das outras
        self.maxQuadrantes = 4
        # Carrega imagem
        self.imagem = Sprite(sprite)
        # Define posição
        self.imagem.position = (randint(0,800/self.maxQuadrantes) + (800/self.maxQuadrantes * (quad%self.maxQuadrantes))), (y)
        # Inicia o retangulo que é a máscara de colisão
        self.colRect = AARectShape(self.imagem.position, (width/2) + 1, (height/2) + 1)

    # Método que checa se algum objeto está colidindo com outro
    def checkOverlap(self,other):
        return self.colRect.overlaps(other)

    # Adiciona valores à posição da fruta
    def addToPosition(self, addX, addY):
        self.imagem.position = self.imagem.position[0] + addX, self.imagem.position[1] + addY
        self.colRect.center = self.imagem.position

    # Retorna valor da coordenada Y na posição da fruta
    def getPositionY(self):
        return self.imagem.position[1]

    def getQuadr(self):
        return self.teste

# Classe de controle que cria as frutas e faz elas cairem
class FruitsControl(Layer):

    is_event_handler = True

    def __init__(self,*arg):
        super(FruitsControl, self).__init__()

        self.fruits = []
        self.listSize = len(arg)
        self.list = arg
        # quadrante de X em que as frutas começam a cair
        self.quadx = 2;
        #posição em y inicial das frutas
        y = 600
        # Carrega frutas em uma lista
        for x in range(1, 5):

            # Coloca no fim da lista uma fruta aleatória
            rand = (randint(1, len(arg)))
            sprite = Sprite (arg[rand-1])
            fruit = Fruit  (arg[rand-1], sprite.width, sprite.height,self.quadx,y)
            # muda a posição em y da próxima fruta a ser instanciada
            y = y + 200
            # Muda o quadrante (x) da fruta
            rand = randint(0,1)
            if  (rand == 0 and self.quadx > 0):
                self.quadx = self.quadx -1;
            elif (rand == 1 and self.quadx < fruit.maxQuadrantes):
                self.quadx = self.quadx + 1
            self.fruits.append(fruit)
            self.add(fruit.imagem)

        # programa a função para ser chamada nos eventos update
        self.schedule(self.fruitFall)


    # Frutas caem
    def fruitFall(self, dt):
        for fruit in self.fruits:
            # aumente o (abs) segundo parâmetro para fazer as frutas caírem mais rápido
            fruit.addToPosition(0,-3)
            # Testa posição das frutas (verifica necessidade de update)
            if (fruit.getPositionY() < 0):
                self.remove(fruit.imagem)
                self.fruits.remove(fruit)
                self.updateFruits()

    # Instancia novas frutas
    def updateFruits(self):
        rand = (randint(1, self.listSize))
        sprite = Sprite (self.list[rand-1])
        fruit = Fruit  (self.list[rand-1], sprite.width, sprite.height,self.quadx, 700)
        rand = randint(0,1)
        if  (rand == 0 and self.quadx > 0):
            self.quadx = self.quadx -1;
        elif (rand == 1 and self.quadx < fruit.maxQuadrantes):
            self.quadx = self.quadx + 1
        self.fruits.append(fruit)
        self.add(fruit.imagem)