# coding=utf-8
# Código por: [insira seu nome]

from main_handler import *
# mude o nome do jogo
game.createWindow("joguinho")
game.createBackground("imagens/background.png")
game.createScore()
# altere a imagem e coloque a que você preferir!
game.createCharacter("imagens/gaia.png")
# coloque quantas "frutas" quiser!
game.createFruits("imagens/maca.png","imagens/pera.png","imagens/banana.png")
game.startScore()
game.startCollision()
run(game)

# Seu código aqui