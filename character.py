# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''

import pyglet
from cocos.layer.base_layers import Layer
from cocos.sprite import Sprite
from cocos.collision_model import AARectShape

# Classe para o personagem e controle
class Character(Layer):

    is_event_handler = True

    def __init__(self, image):
        super(Character, self).__init__()

        # Inicia variaveis de movimento
        self.move_right_key = False
        self.move_left_key = False

        # Carrega imagem e define sua posição na tela
        self.char_imagem = Sprite(image)
        self.char_imagem.position = 400,95

        # Adiciona imagem ao layer
        self.add(self.char_imagem)

        # Programa para rodar aos eventos de update
        self.schedule(self.moveCharacter)

        # Inicia retangulo de colisão
        self.colRect = AARectShape(self.char_imagem.position, (self.char_imagem.width/2), (self.char_imagem.height/2));

    # Evento de aperto de tecla, diz qual movimento acontece
    def on_key_press(self, key, modifiers):
        # Se apertou direcional direito, move para direita independente de estar apertando esquerda
        if(key == pyglet.window.key.RIGHT):
            self.move_right_key = True
            self.move_left_key = False
        # Se apertou direcional esquerdo, move para esquerda independente de estar apertando direita
        elif(key == pyglet.window.key.LEFT):
            self.move_left_key = True
            self.move_right_key = False

    # Evento de soltar a tecla, diz para parar movimentação
    def on_key_release(self, key, modifiers):
        # Ao soltar a respectiva tecla, para o movimento
        if (key == pyglet.window.key.RIGHT):
            self.move_right_key = False
        elif (key == pyglet.window.key.LEFT):
            self.move_left_key = False

    # Função chamada nos eventos de update para movimentar o personagem
    def moveCharacter(self,dt):
        if(self.move_right_key):
            self.moveRight()
        elif(self.move_left_key):
            self.moveLeft()
        self.colRect.center = self.char_imagem.position

    # Movimenta personagem para a esquerda
    def moveRight(self):
        if(self.char_imagem.position[0] < 745):
            # aumente quanto soma à posição 0 para deixar o personagem mais rápido
            self.char_imagem.position = self.char_imagem.position[0]+8,self.char_imagem.position[1]

    # Movimenta personagem para a direita
    def moveLeft(self):
        if(self.char_imagem.position[0] > 55):
            # aumente quanto soma à posição 0 para deixar o personagem mais rápido
            self.char_imagem.position = self.char_imagem.position[0]-8,self.char_imagem.position[1]

    # TODO: Colisão para fazer com as frutas