# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''

from cocos.director import director
from cocos.scene import Scene
from game_window import GameWindow
from background import Background
from character import Character
from fruits import FruitsControl
from score import Score


# Módulo para funções gerais para uso no código do jogo
class MainHandler():
    def __init__(self):
        # Inicia variaveis vazias
        self.main_scene = None
        self.main_window = None
        self.bkg = None
        self.titulo = None
        self.character = None
        self.points = 0
        self.scoreControl = 0

    # Cria uma janela vazia
    def createWindow(self, titulo):
        # Inicia diretor
        window = director.init(width=800, height=600, caption=titulo, fullscreen=False)

        # Instancia a janela e sua cena
        self.main_window = GameWindow()
        self.main_scene = Scene(self.main_window)

    # Cria e adiciona o background na cena
    def createBackground(self, img):
        self.bkg = Background(img)
        self.main_scene.add(self.bkg, z=1)

    def createScore(self):
        self.score = Score(self.points)
        self.main_scene.add(self.score, z=3)

    # Cria e adiciona personagem numa posição inicial
    def createCharacter(self, img):
        self.character = Character(img)
        self.main_scene.add(self.character, z=2)

    # Cria frutas e faz elas caírem
    def createFruits(self,*arg):
        self.food = FruitsControl(*arg)
        self.main_scene.add(self.food, z=2)

    # Checa colisão
    def checkCollision(self, dt):
        if(len(self.food.fruits) > 0):
            for fruit in self.food.fruits:
                if(fruit.checkOverlap(self.character.colRect)):
                    self.food.remove(fruit.imagem)
                    self.food.fruits.remove(fruit)
                    self.food.updateFruits()
                    # altere para mudar a quantidade de pontos por cada fruta coletada
                    self.points = self.points+10

    def updateScore(self, dt):
        
        if (self.scoreControl != self.points):
            self.scoreControl = self.points
            self.score.scoreUpdate(self.points)

    def startScore(self):
        self.main_scene.schedule(self.updateScore)


    # Começa a checar por colisão nos eventos update
    def startCollision(self):
        self.main_scene.schedule(self.checkCollision)



# Função para rodar o jogo
def run(handler):
    director.run(handler.main_scene)

# Instancia o handler para usar no arquivo do jogo
game = MainHandler()