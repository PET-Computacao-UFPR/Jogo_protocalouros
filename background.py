# coding=utf-8
'''
 Código por PET-Computação-UFPR
'''

from cocos.layer.base_layers import Layer
from cocos.sprite import Sprite

# Classe que cuida do fundo do jogo
class Background(Layer):
    def __init__(self, image):
        super(Background, self).__init__()

        # Carrega imagem do fundo e centraliza
        bkg_imagem = Sprite(image)
        bkg_imagem.position = 400,300

        # Adiciona o fundo ao layer
        self.add(bkg_imagem)